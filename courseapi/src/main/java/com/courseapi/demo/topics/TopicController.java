package com.courseapi.demo.topics;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TopicController {
	@Autowired
	private TopicServeses topicserveses;
	@RequestMapping("/topics")
	public List<Topics> getALLTopic() {
		return topicserveses.getAllTopics();
	}
	@RequestMapping("/topics/{id}")
	public Topics getTopic(@PathVariable String id) {
		return topicserveses.getTopic(id);
	}
	@RequestMapping(method=RequestMethod.POST, value="/topics")
	public void addTopic(@RequestBody Topics topics) {
		topicserveses.addTopic(topics);
	}
	@RequestMapping(method=RequestMethod.PUT, value="/topics/{id}")
	public void updateTopic(@RequestBody Topics topics,@PathVariable String id) {
		topicserveses.updateTopic(topics, id);
	}
	@RequestMapping(method=RequestMethod.DELETE, value="/topics/{id}")
	public  void deleteTopic(@PathVariable String id) {
		topicserveses.deleteTopic(id);
	}
}
