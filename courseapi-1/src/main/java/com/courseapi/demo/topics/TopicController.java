package com.courseapi.demo.topics;


import java.util.List; 

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;



@RestController
@CrossOrigin(origins = {"http://localhost:3000/Topicss"})
public class TopicController {
	@Autowired
	private TopicServeses topicserveses;
	@Autowired
	private TopicRepository topicRepository;
	@RequestMapping("/topics")
	public ResponseEntity< List<Topics>> getALLTopic() {
		return new ResponseEntity<List<Topics>>(topicserveses.getAllTopics(),HttpStatus.OK);
	}
	@RequestMapping("/topics/{id}")
	public ResponseEntity<Object> getTopic(@PathVariable String id) {
		return new ResponseEntity<Object>(topicserveses.getTopic(id),HttpStatus.OK);
	}
	@RequestMapping(method=RequestMethod.POST, value="/topics/{id}")
	public ResponseEntity<String> addTopic(@RequestBody Topics topics,@PathVariable String id) {
		 topicserveses.addTopic(topics);
		if(topicRepository.findById(id)==null) {
			
				
				return new ResponseEntity<String>("Account Created Sucessfully",HttpStatus.CREATED);
			
			
		}else {
			return new ResponseEntity<String>("Account exceted",HttpStatus.OK);
		}
		
	}
	@RequestMapping(method=RequestMethod.PUT, value="/topics/{id}")
	public ResponseEntity<String>updateTopic(@RequestBody Topics topics,@PathVariable String id) {
		
		if(topicRepository.findById(id)==null) {
			return new ResponseEntity<String>("Account not found",HttpStatus.NOT_FOUND);
		}else {
			topicserveses.updateTopic(topics, id);
			return new ResponseEntity<String>("Account Updated",HttpStatus.OK);
			
		}
	}
	@RequestMapping(method=RequestMethod.DELETE, value="/topics/{id}")
	public  ResponseEntity<String> deleteTopic(@PathVariable String id) {
		
		
		if(topicRepository.findById(id)==null) {
			return new ResponseEntity<String>("Account not found",HttpStatus.NOT_FOUND);
		}else {
			topicserveses.deleteTopic(id);

			return new ResponseEntity<String>("Account Deleted Sucessfully",HttpStatus.OK);
			
		}
		
	}
}
