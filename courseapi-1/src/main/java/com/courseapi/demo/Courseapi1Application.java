package com.courseapi.demo;

import org.springframework.boot.SpringApplication;

import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class Courseapi1Application {
	
	public static void main(String[] args) {
		SpringApplication.run(Courseapi1Application.class, args);
	}

}
