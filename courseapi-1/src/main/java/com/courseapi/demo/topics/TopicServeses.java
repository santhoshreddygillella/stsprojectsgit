package com.courseapi.demo.topics;

import java.util.ArrayList;


import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TopicServeses {
	@Autowired
	private TopicRepository topicRepository;
	
   /*List<Topics> topicList=new ArrayList<>(Arrays.asList(
			new Topics("java","collection","corejava"),
			new Topics("advsjava","jdbc,servlets","fullStack"),
			new Topics("moreadvance","spring,Hybernet","springFramwork")
			));*/
   
   public List<Topics> getAllTopics(){
	 //  return topicList;
	   List<Topics> topics=new ArrayList<Topics>();
	   topicRepository.findAll()
	   .forEach(topics::add);
	   return topics;
   }
     
   public Object getTopic(String id) {
	   //return topicList.stream().filter(t ->t.getTid().equals(id)).findFirst().get();
	   return topicRepository.findById(id);
   }

public void addTopic(Topics topics) {
	topicRepository.save(topics);
	
}

public void updateTopic(Topics topics, String id) {
	
	topicRepository.save(topics);
		
	}

	


public void deleteTopic(String id) {
	topicRepository.deleteById(id);
}
   
	
}
