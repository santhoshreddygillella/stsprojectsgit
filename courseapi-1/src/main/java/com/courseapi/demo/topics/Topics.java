package com.courseapi.demo.topics;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="courses")
	public class Topics {
	@Id
		private String tid;
		private String tname;
		private String discreption;
		public String getTid() {
			return tid;
		}
		public void setTid(String tid) {
			this.tid = tid;
		}
		public String getTname() {
			return tname;
		}
		public void setTname(String tname) {
			this.tname = tname;
		}
		public String getDiscreption() {
			return discreption;
		}
		public void setDiscreption(String discreption) {
			this.discreption = discreption;
		}
		public Topics() {
			
		}
		public Topics(String tid, String tname, String discreption) {
			super();
			this.tid = tid;
			this.tname = tname;
			this.discreption = discreption;
		}
		
		

	}



