package com.courseapi.demo.topics;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class TopicServeses {
	
   List<Topics> topicList=new ArrayList<>(Arrays.asList(
			new Topics("java","collection","corejava"),
			new Topics("advsjava","jdbc,servlets","fullStack"),
			new Topics("moreadvance","spring,Hybernet","springFramwork")
			));
   
   public List<Topics> getAllTopics(){
	   return topicList;
   }
     
   public Topics getTopic(String id) {
	   return topicList.stream().filter(t ->t.getTid().equals(id)).findFirst().get();
   }

public void addTopic(Topics topics) {
	topicList.add(topics);
	
}

public void updateTopic(Topics topics, String id) {
	
	for(int i=0;i <topicList.size();i++) {
		Topics t=topicList.get(i);
		if(t.getTid().equals(id)) {
			topicList.set(i, topics);
			return;
		}
		
	}

	
}

public void deleteTopic(String id) {
	topicList.removeIf(t ->t.getTid().equals(id));
}
   
	
}
